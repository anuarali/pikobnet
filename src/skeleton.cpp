/**
 * This part will be the header file declarations in package, but <Rcpp::sourceCpp()> requires single file to work, so it is here for now
 */

// Pure C++
#include <numeric>
#include <bitset>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <limits>

// Pure C99
#include <cstring>
#include <cmath>
#include <limits.h>
#include <cstdio>
#include <cstdint>

/**
 * We include the external library of RcppArmadillo,
 * this allows for a future usage of linear algebra, advanced Rcpp, etc...
 *
 * Next, we include the external library of RcppParallel for low-level C++ multithreading
 *
 * NOT USED AS FOR NOW
 */
// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>
#include <RcppArmadilloExtensions/sample.h>
//#include <Rcpp.h>

/**
 * Most used Rcpp objects, used to remove Rcpp prefix
 */
using Rcpp::Rcout;
using Rcpp::Rcerr;
using Rcpp::NumericVector;
using Rcpp::NumericMatrix;
using Rcpp::DataFrame;
using Rcpp::Nullable;
using Rcpp::LogicalMatrix;
using Rcpp::IntegerMatrix;
using Rcpp::List;
using Rcpp::Function;
using Rcpp::IntegerVector;
using Rcpp::_;

inline static double RNegInf = -std::numeric_limits<double>::infinity();

/**
 * Also -- include the "saveRDS" function for partial result serialization and save
 */
Rcpp::Environment base("package:base");
Function saveRDS = base["saveRDS"];
Function sumMat = base["sum"];


/**
 * A Log-Sum-Exp trick for two very low log values to prevent overflow/underflow
 */
double logSumExp(double x, double y)
{
  double mx = (x<y)?y:x;

  double xr = x - mx;
  double yr = y - mx;


  return mx + log(
      exp(xr) + exp(yr)
    );

}




/**
 * This part will be the separate header file for tuple + hash + Trie data structures
 */
#include <tuple>

/**
 * We will encode a 3D coordinate:
 *   > listID --> ID of the partition of our N-partition graph, goes: (0, N-2)
 *       Note: because, for example, for 3 partition(A,B,C), we have 2 adjacency matrices: AB, BC
 *   > FROM, TO  --> row, column ID of the particular
 */
typedef std::tuple<uint8_t, uint16_t, uint16_t> key_tp;

enum ChangeType
{
  ADD = true,
  DELETE = false
};

struct IData
{
  const List* PK;
  const List* SK;
  const IntegerVector* sizes;
  uint32_t N;
  const List* param_in;

  Function* score_full_func;
  Function* score_change_func;
};
struct ISettings
{
  uint32_t I;
  bool debug;
};

#define SAVE_FREQ 500
#define PRINT_FREQ 1000
#define PARTIAL_ADD_ONLY "[pSK-SS][add only]"
#define PARTIAL_ADD_REMOVE "[pSK-SS][add-remove]"
#define SKSS_PHASE "[SK-SS][add-remove]"

class Network
{
protected:
  IData input_data;
  ISettings settings;

  /**
   * sort individual elements from lowest to largest by unordered_set
   * this will be used to push to TRIE -> count subgraphs
   */
  std::vector<key_tp> ord_V_G;


  /**
   * store the current adjacency graph and the best MAP model found during the search
   *   > update only when better model is found
   */
  std::vector<LogicalMatrix*> G;
  std::vector<LogicalMatrix*> G_sampled;

  std::vector<double> acceptedScores;
  std::vector<double> acceptedScoresPrior;

  uint32_t current_id = 0;

  double probabilityMaxOptim = R_NegInf;
  std::vector<LogicalMatrix*> G_optim;

public:
  Network(IData input, ISettings settings)
  {
    this->input_data = input;
    this->settings = settings;

    this->initializeLists();
  }

  ~Network()
  {
    this->deleteLists();
  }

  void partialSK_SS()
  {
    for(uint32_t i = 0; i < 1; i++)
    {
      Rcout << "partialSK-SS, Iteration: " << i << "\n";
      //this->emptyCurrCountsAndG();
      this->partialSK_SS_iter();
      //this->ord_V_G.clear();
    }
  }

  void SK_SS()
  {

    /*
    std::vector<
      std::vector<LogicalMatrix*>
      > all_G_sampled;

    for(uint32_t i = 0; i < this->settings.I; i++)
    {
      std::vector<LogicalMatrix*> to_sample = std::vector<LogicalMatrix*>();
      const List* SK_list = this->input_data.SK;

      for(uint32_t i = 0; i < SK_list->length(); i++)
      {
        const IntegerMatrix& sk = (*SK_list)[i];
        to_sample.push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
        Rcpp::rownames(*(to_sample[i])) = Rcpp::rownames(sk);
        Rcpp::colnames(*(to_sample[i])) = Rcpp::colnames(sk);
      }

      this->V_G.SampleAdjacencyMatrix(to_sample);
      all_G_sampled.push_back(to_sample);
    }
    */
    for(uint32_t i = 0; i < this->settings.I; i++)
    {
      this->partialSK_SS();
      Rcout << "SK-SS, Iteration: " << i << "\n";
      //this->ord_V_G.clear();
      //this->resetSampledG();
      //this->G_sampled = all_G_sampled[i];

      //this->emptyCurrCountsAndG();
      this->SK_SS_phase2_iter();
    }

    Rprintf("End of SK_SS() function here\n");
  }

  List& result()
  {
    Rprintf("Optimum log probability: %.10f\n", this->probabilityMaxOptim);

    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G_optim[i];
    }

    return *ret;
  }

protected:
  void SK_SS_phase2_iter()
  {
    Rprintf("phase 2\n");
    List twoScores = this->callScoreFull();
    const List* SK_list = this->input_data.SK;

    IntegerVector riter = Rcpp::sample(
      this->input_data.N, (this->input_data.N));

    for(uint32_t i = 0; i < riter.size(); i++)
    {
      this->current_id = i;

      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;

      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];
        sumSizes += sizeMatrix;

        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }

      const IntegerMatrix& sk = (*SK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();

      if(i % PRINT_FREQ == 0) // i % PRINT_FREQ == 0
      {
        this->DebugInfo(SKSS_PHASE, i);
      }

      LogicalMatrix& gz = *(this->G[lindex]);

      if(gz(r,c) == 0)  // if not in G -> try add
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD);
      else              // if in G -> try delete
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::DELETE);
    }

    Rprintf("end of phase 2\n");
  }


  void partialSK_SS_iter()
  {
    List twoScores = this->callScoreFull();
    const List* SK_list = this->input_data.SK;
    const List* PK_list = this->input_data.PK;

    IntegerVector riter = Rcpp::sample(
      this->input_data.N, this->input_data.N);

    Rprintf("partialSKSS-add-only phase\n");
    uint32_t tst = 0;

    for(uint32_t i = 0; i < this->input_data.N; i++)
    {
      this->current_id = i;

      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;

      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];

        sumSizes += sizeMatrix;

        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }

      const IntegerMatrix& sk = (*SK_list)[lindex];
      const IntegerMatrix& pk = (*PK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();

      if(i % (PRINT_FREQ) == 0)
      {
        this->DebugInfo(PARTIAL_ADD_ONLY, i);
      }

      if(
         (sk(r, c) != 1))
      {
        continue;
      }

      tst++;

      LogicalMatrix& gz = *(this->G[lindex]);

      if(gz(r,c) == 1){  // if in G -> go next, add only phase
        continue;
      }


      this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD);

    }
    Rprintf("After iteration of pSK-SS add only i have: %d edges\n", tst);

    double theor = twoScores["score"];

    Rprintf("Final score of (theoretically) prior is: %f\n", theor);
    tst = 0;
    Rprintf("partialSKSS-add-remove phase\n");
    riter = Rcpp::sample(
      this->input_data.N, this->input_data.N);

    for(uint32_t i = 0; i < this->input_data.N; i++)
    {
      uint32_t flatindex = riter[i] - 1;
      uint32_t lindex = 0;
      uint32_t xyindex = flatindex;

      uint32_t sumSizes = 0;
      while(true)
      {
        uint32_t sizeMatrix = (*this->input_data.sizes)[lindex] * (*this->input_data.sizes)[lindex + 1];

        sumSizes += sizeMatrix;

        if(sumSizes > flatindex)
        {
          break;
        }
        lindex++;
        xyindex -= sizeMatrix;
      }

      const IntegerMatrix& sk = (*SK_list)[lindex];
      const IntegerMatrix& pk = (*PK_list)[lindex];
      uint32_t r = xyindex % sk.nrow();
      uint32_t c = xyindex / sk.nrow();

      if(i % (PRINT_FREQ) == 0)
      {
        this->DebugInfo(PARTIAL_ADD_REMOVE, i);
      }

      if(
         (sk(r, c) != 1))
      {
        continue;
      }

      LogicalMatrix& gz = *(this->G[lindex]);

      if(gz(r,c) == 0)  // if not in G -> try add
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::ADD);
      else              // if in G -> try delete
        this->tryPerformMove(&twoScores, lindex, r, c, ChangeType::DELETE);
    }
  }

  void DebugInfo(std::string phase, uint32_t iter)
  {
    Rprintf("%s[%5u]/[%5u][Score:%9.6f]\n", //
            phase.c_str(), iter, this->input_data.N,
            this->probabilityMaxOptim
              );
  }

  List move_to_list(uint32_t listID, uint32_t from, uint32_t to, bool action)
  {
    List L = List::create(_["listID"] = listID,
                          _["from"] = from,
                          _["to"] = to,
                          _["action_is_add"] = action);
    return L;
  }

  List& G_to_list()
  {
    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G[i];
    }

    return *ret;
  }

  List& G_optim_to_list()
  {
    const List* SK_list = this->input_data.SK;
    List* ret = new List(SK_list->length());

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      (*ret)[i] = *this->G_optim[i];
    }

    return *ret;
  }

  void resetSampledG()
  {
    const List* SK_list = this->input_data.SK;

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];

      *(this->G_sampled)[i] = LogicalMatrix(sk.nrow(), sk.ncol());
      Rcpp::rownames(*(this->G_sampled)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_sampled)[i]) = Rcpp::colnames(sk);
    }
  }

  void emptyCurrCountsAndG()
  {
    const List* SK_list = this->input_data.SK;

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];

      *(this->G)[i] = Rcpp::LogicalMatrix(
                        Rcpp::clone(*this->G_sampled[i])
                      );
      Rcpp::rownames(*(this->G)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G)[i]) = Rcpp::colnames(sk);
    }
  }

  /**
   * We should convert Rcpp lists to vectors of matrices because:
   *    > we can't easily manipulate Rcpp List objects
   *    > with matrix pointers it is easier to manage memory manually
   *    > BUT! we should still use matrices to call R score functions
   */
  void initializeLists()
  {
    const List* SK_list = this->input_data.SK;

    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      const IntegerMatrix& sk = (*SK_list)[i];

      (this->G).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G)[i]) = Rcpp::colnames(sk);

      (this->G_optim).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G_optim)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_optim)[i]) = Rcpp::colnames(sk);

      (this->G_sampled).push_back(new LogicalMatrix(sk.nrow(), sk.ncol()));
      Rcpp::rownames(*(this->G_sampled)[i]) = Rcpp::rownames(sk);
      Rcpp::colnames(*(this->G_sampled)[i]) = Rcpp::colnames(sk);
    }

  }

  /**
   * Clear matrices in vectors
   */
  void deleteLists()
  {
    const List* SK_list = this->input_data.SK;
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      delete this->G[i];
      delete this->G_optim[i];
    }

    this->G.clear();
    this->G_optim.clear();
  }


  void tryPerformMove(List* score, uint32_t listID, uint32_t from, uint32_t to, ChangeType action,
                      bool shouldUpdateTrie = true)
  {
    // Compute the possible score
    List scoreChanges = this->callScoreChange(score, listID, from, to, action);
    /**
     * Modified(explicit per-partes) is used, e.g.:
     *
     * since we are working in small precisions + log scale:
     *   > difference is calculated in SCORE functions manually
     *   > difference is separately calculated for Likelihood / Prior part
     *   > the previous is because they have orders of magnitude differences
     *   > ONLY afterwards the summed difference is converted by exp, since it is tractable!
     */
    double log_diff = scoreChanges["diff"];
    double diff = exp(log_diff);
    bool PASSED = false;
    if(!arma::is_finite(diff))
    {
	PASSED = log_diff > 0.0;  // any positive Inf is sure pass
				  // any negative Inf is sure fail
    }
    else{
	NumericVector probs;

    // Convert the log difference to B/A and normalize it
    // normalize(1, B/A) = normalize(A,B)
    // that way we get probabilities for two candidates!
	probs = {1.0, diff};
    	probs = probs / Rcpp::sum(probs);

	IntegerVector ids = {0, 1};
    	IntegerVector ret = Rcpp::sample(ids, 1, probs = probs);

	PASSED = (ret[0] == 1);
    }

    // If the first was sampled, skip
    if(!PASSED){
	    return;
    }

    // Update score
    *score = scoreChanges;

    this->UpdateVisited(scoreChanges);

    // Update G if not in SK-SS part
    bool isNew = this->makeMove(listID, from, to, action, shouldUpdateTrie);

    this->CheckVisitedGraphOptimum(scoreChanges);
  }

  void UpdateVisited(List& scoreChanges)
  {
    List prior = scoreChanges["prior"];
    List likelihood = scoreChanges["likelihood"];

    double pri = prior["score"];
    double lik = likelihood["score"];

    this->acceptedScores.push_back(lik);
    this->acceptedScoresPrior.push_back(pri);
  }

  bool makeMove(uint32_t listID, uint32_t from, uint32_t to, ChangeType action, bool shouldUpdateTrie)
  {
    // update adjacency
    (*(this->G[listID]))(from,to) = action == ChangeType::ADD;

    return true;
  }

  void CheckVisitedGraphOptimum(List& scoreChanges)
  {
    List prior = scoreChanges["prior"];
    List likelihood = scoreChanges["likelihood"];

    double pri = prior["score"];
    double lik = likelihood["score"];

    double score = scoreChanges["score"];

    this->acceptedScores.push_back(lik);
    this->acceptedScoresPrior.push_back(pri);

    if(score <= this->probabilityMaxOptim)
    {
      return;
    }

    this->probabilityMaxOptim = score;

    const List* SK_list = this->input_data.SK;
    for(uint32_t i = 0; i < SK_list->length(); i++)
    {
      *this->G_optim[i] = Rcpp::LogicalMatrix(
        Rcpp::clone(*this->G[i])
      );
    }
  }



  List callScoreChange(List* score, uint32_t listID, uint32_t from, uint32_t to, ChangeType action)
  {
    List& adjs = this->G_to_list();
    List move = this->move_to_list(listID, from, to, action == ChangeType::ADD);

    List scoreChanges = (*this->input_data.score_change_func)
      (
          move,
          adjs,
          *score);

    delete &adjs;

    return scoreChanges;
  }

  List callScoreFull()
  {
    List& adjs = this->G_to_list();
    List twoScores = (*this->input_data.score_full_func)(
      adjs,
      *this->input_data.param_in
    );
    delete &adjs;
    return twoScores;
  }
};

/**
 * This function is the interface to the R part, the only function exported
 */
// [[Rcpp::export]]
List c_SK_SS(
    const List& PK,
    const List& SK,
    const IntegerVector& sizes,
    const List& param,
    uint32_t I,
    bool debug,
    Function score_full_func,
    Function score_change_func
)
{
  uint32_t N = 0;
  for(uint32_t i = 0; i < (sizes.size() - 1); i++)
  {
    N += sizes[i] * sizes[i+1];
  }
  Rprintf("N = %u\n", N);

  IData i_data;

  i_data.PK = &PK;
  i_data.SK = &SK;
  i_data.sizes = &sizes;
  i_data.param_in = &param;
  i_data.N = N;

  i_data.score_full_func = &score_full_func;
  i_data.score_change_func = &score_change_func;

  ISettings settings;

  settings.I = I;
  settings.debug = debug;

  Network* net = new Network(i_data, settings);

  net->SK_SS();
  List ret = net->result();
  delete net;

  return ret;
}
