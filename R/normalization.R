

#' @name normalize.data
#' @title Normalize data matrix
#' @description
#'  Normalize raw read counts according to one of simple normalization methods. Note that the normalization does not perform the \emph{Per million} multiplication, e.g., it does not multiply the normalized value by \eqn{10^6}
#' @param data A matrix or dataframe of N sample rows and n variable columns. Each value correspond to a raw read count
#' @param type A type of normalization to be used. Select one of Count per Milion (CPM), Transcripts Per Million (TPM) and Fragments Per Kilobase Million (FPKM)
#' @param geneLength A vector of size n corresponding to variable (RNA) count. Each element corresponds to the length of RNA in nucleotides
#' @returns A normalized matrix of the same size
#'
#'
#' @export
normalize.data <- function(data, type=c("CPM", "TPM", "FPKM"),
                           partition,
                           geneLength=NULL){
    ret = as.matrix(data)
    type = match.arg(type)
    if(is.null(geneLength) == FALSE){
        stopifnot(nrow(data) == length(geneLength))
    }

    for(part in partition){
        subret = ret[part,]
        subgeneLength = geneLength[part]

        if(type == "CPM"){
            subret = t(t(subret) / colSums(subret))
        }
        else if(type == "FPKM"){
            subret = t(t(subret) / colSums(subret))
            if(is.null(geneLength) == TRUE){
                stop("FPKM is used without geneLength provided (NULL)!")
            }
            subret = subret / subgeneLength
        }
        else if(type == "TPM"){
            if(is.null(subgeneLength) == TRUE){
                stop("TPM is used without geneLength provided (NULL)!")
            }
            subret = (subret / subgeneLength)
            subret = t(t(subret) / colSums(subret))
        }
        else{
            stop("Unknown type: ", type)
        }
        ret[part,] = subret
    }
    ret = data.frame(ret, check.names = FALSE)


    return(ret)
}

to.formula <- function(node, parents, offsetNode=NULL){
    frml <- ""
    if(length(parents) == 0){
        frml <- paste("`", node, "`", " ~ ",1, sep="")
    }
    else{
        frml <- paste("`", node, "`", " ~ ",
                      paste("`", parents, "`", collapse = "+", sep=""),
                      sep="")
    }

    if(is.null(offsetNode) == FALSE){
        frml <- paste(frml, " + ", "offset(", offsetNode, ")", sep="")
    }
    frml <- as.formula(frml)
    return(frml)
}
