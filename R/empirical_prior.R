#' @rdname empiricalPrior
#' @title An empirical score for the prior part of the SK-SS
#' @description asd
#'
EmpiricalPrior_param_init <- function(...)
{
  additional_params <- list(...)
  PKs <- additional_params$PKs
  beta_L <- additional_params$beta_L
  beta_H <- additional_params$beta_H

  K <- sum(unlist(lapply(PKs, function(l) sum(l != 0.5))))

  param_in <- list(score_func = EmpiricalPrior_score,
                   change_func = EmpiricalPrior_change,
                   PKs=PKs, energy=0, beta_L=beta_L, beta_H=beta_H, K=K)
  return(param_in)
}


#'
#' Compute a score-P(G) for a given graph. Formula (by Isci):
#' E(i,j) =
#'        (if edge in G) = 1 - B(i,j)
#'
#'    (if edge not in G) = B(i,j)
#'
#' E(G) = sum(i,j) E(i,j)
#'
#' For theoretical reference see:
#' * IntOMICS:
#' https://doi.org/10.1186/s12859-022-04891-9
#' * Bayesian prior see: (TODO remove, change for some more adequate ones :)   )
#' https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957076/
#'
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param B_PKs A list of prior matrices for each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param beta_L A lower threshold on prior's weight      (numeric)
#' @param beta_H An upper threshold on prior's weight     (numeric)
#' @returns A list with 2 values, score-P(G) and energy U(G)
#'
#'


#' @rdname empiricalPrior
EmpiricalPrior_score <- function(G_adjs, param)
{
  # Extract input
  B_PKs <- param$PKs
  beta_L <- param$beta_L
  beta_H <- param$beta_H
  K <- param$K

  total_energy <- 0
  for(i in 1:length(G_adjs))
  {
    # IntOMICS score:
    # if edge included ----> (1 - b_i_j)
    # if edge no included -> (b_i_j)
    adj <- G_adjs[[i]]
    bpk <- B_PKs[[i]]
    bpk <- bpk - 0.5
    bpk <- bpk * 2

    adj <- G_adjs[[i]]
    bpk <- B_PKs[[i]]
    adj <- adj[bpk != 0.5]
    bpk <- bpk[bpk != 0.5]
    local_energy <- bpk * (1 - adj) + (1 - bpk) * adj
    local_energy <- sum(local_energy)

    total_energy <- total_energy + local_energy
  }
  beta <- beta_H
  sP_G <- fast_point_log_P_G(beta, K, total_energy)

  # Update param
  param$score <- sP_G
  param$prev_score <- param$score
  param$diff <- 0
  param$energy <- total_energy

  return(param)
}


#' Compute a score~proportional to~ log P(G) for a given graph. This part of function should be called
#'
#' For an integral formula of the Bayesian prior see (TODO replace by more sophisticated):
#' https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957076/
#'
#' The formula used below is the closed-form MAP solution with given Beta_L, Beta_H
#'
#'



#' @rdname empiricalPrior
EmpiricalPrior_change <- function(move, G_adjs, param)
{
  # Extract input
  part_id <- move$listID
  from <- move$from
  to <- move$to
  action_is_add <- move$action_is_add
  B_PKs <- param$PKs
  beta_L <- param$beta_L
  beta_H <- param$beta_H
  K <- param$K
  prev_score <- param$prev_score
  prev_energy <- param$energy

  # Compute change in energy
  B_PK_mat <- B_PKs[[part_id]]
  b_i_j <- B_PK_mat[from, to]
  u_change <- 1 - 2 * b_i_j
  u_change <- (2 * action_is_add - 1) * u_change
  total_energy <- prev_energy + u_change


  # Compute change in prior
  beta <- beta_H
  sP_G <- fast_point_log_P_G(beta, K, total_energy)

  # Update param
  param$diff <- sP_G - prev_score
  param$score <- sP_G
  param$prev_score <- param$score
  param$energy <- total_energy

  return(param)
}


# TODO remove/incorporate
P_G_neighbours_change <- function(G_adjs, part_id, from, to, action_is_add)
{
  prev_number <- sum(G_adjs[[part_id]][,to]) # + 1
  new_number <- prev_number + (2 * action_is_add - 1)


  possible_parents <- nrow(G_adjs[[part_id]])

  #prev_number <- -log(prev_number)
  #new_number <- -log(new_number)

  prev_number <- -f_log_combi(n=possible_parents, k=prev_number)
  new_number <- -f_log_combi(n=possible_parents, k=new_number)

  change <- new_number - prev_number
  return(change)
}

P_G_neighbours <- function(G_adjs)
{
  score_ret <- sum(unlist(lapply(G_adjs, function(adj){

    neights <- colSums(adj)
    possible_parents <- nrow(adj)

    #neights <- neights + 1  # very small penalty

    neights <- sapply(neights, function(nt) -f_log_combi(n=possible_parents, k=nt))
    sum(neights)

    #-log(neights)
  })))
  return(score_ret)
}









#'
#' For the derivation of approximation see:
#' https://math.stackexchange.com/questions/64716/approximating-the-logarithm-of-the-binomial-coefficient
#'
f_log_combi <- function(n, k)
{
  if((k == 0) || (k == n)) return(0)

  # For tractable (inside the INT32 range) inputs we compute the precise number
  if(n < 30)
  {
    ret <- asnumeric(binom(n, k))
    ret <- log(ret)
    return(ret)
  }

  # Alternatively:
  # log(x, base=exp(1)) returns wrong numbers
  # loge ---> NO, for large numbers (f.e. 45489, 200) it returns NA!!!
  #ret <- binom(n, k)
  #ret <- log(ret, base=exp(1))
  #ret <- asnumeric(ret)
  #exact_num <- ret

  # Otherwise we use the Stirling's approximation (very close, O(1/n, 1/k)):
  ret <- n * log(n) - k * log(k) - (n-k) * log(n - k)
  ret <- ret + 0.5 * (log(n) - log(k) - log(n - k) - log(2 * pi))

  return(ret)
}

possible_log_z_b <- function(beta, K)
{
  ret <- K * log(1 + exp(-beta))
  return(ret)



  # inps <- 0:K
  # inps <- sapply(inps, function(i) {
  #   ret1 <- f_log_combi(K, i)
  #   #ret2 <- (N - K) * 0.5 + i
  #   ret2 <- i * beta
  #   ret <- ret1 - ret2
  #   return(ret)
  # })
  # inps <- logSumExp(inps)
  #
  # cat(ret, " x ", inps, "\n")
  # return(inps)
}

fast_point_log_P_G <- function(beta, K, E)
{
  log_z_b <- possible_log_z_b(beta, K)
  ret <- -beta * E - log_z_b
  return(ret)
}

myclamp <- function(val, lower, upper)
{
  if(val < lower) return(lower)
  if(val > upper) return(upper)
  return(val)
}


optimum_beta <- function(E, K, beta_L, beta_H)
{
  # If E == 0, can't divide by 0, take
  if(E == 0) return(beta_H)

  ret <- K / E - 1
  ret <- log(ret)
  ret <- myclamp(ret, lower=beta_L, upper=beta_H)
  return(ret)
}

marginalization_P_G <- function(E, K, beta_L, beta_H, delta_beta=0.1)
{
  betas <- seq(beta_L, beta_H, delta_beta)

  ret <- sapply(betas, function(b) fast_point_log_P_G(b, K, E))

  ret <- logSumExp(ret)

  ret <- ret - log(beta_H - beta_L) + log(delta_beta)

  return(ret)
}

# Remains of my comprehensive numerical tests with hypergeometric functions
#library(gsl)
#library(hypergeo)
#' No idea what is happening here,
#' but here is the numerical trick to be sure that hypergeom is always positive
#' https://stats.stackexchange.com/questions/33451/computation-of-hypergeometric-function-in-r
#'
# Gauss2F1b <- function(a,b,c,x){
#   if(x>=0 & x<1){
#     hyperg_2F1(a,b,c,x)
#   }else{
#     hyperg_2F1(a,c-b,c,1-1/(1-x))/(1-x)^a
#   }
# }

biological_prior <- function(G_adjs, B_PKs, N)
{
  K <- sum(unlist(lapply(B_PKs, sum)))
  E <- 0 # TODO compute
  #first_part <- hypergeo_cover2(1 + E, K, 2 + E, -1);
  #first_part <- f15.3.1(1 + E, K, 2 + E, -1);
  #first_part <- Re(first_part)
  return(log(first_part))
}


#'
#' Compute a prior energy function of the given adjacency graph. Formula:
#' E(i,j) =
#'        (if edge in G) = 1 - B(i,j)
#'
#'    (if edge not in G) = B(i,j)
#'
#' E(G) = sum(i,j) E(i,j)
#'
#' For reference see:
#' * IntOMICS:
#' https://doi.org/10.1186/s12859-022-04891-9
#' * Bayesian prior see:
#' https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957076/
#'
#' @param G_adjs A list of adjacency matrices across each partition interaction, for example: list(CIRC->MI, MI->M)
#' @param B_PKs A list of prior matrices for each partition interaction, for example: list(CIRC->MI, MI->M), ! modified to scores [1,0.5,0]
#' @param N A total number of genes
#' @returns E(G), the prior energy function for a given graph
#'
Energy_compute <- function(G_adjs, B_PKs, N)
{
  E <- 0

  # we should count blocked edges too,
  # they have 0 prior probability -> E(i,j) = 1
  # we do it by doing (N x N) - (non-blocked number)
  E_non_blocked <- 0

  for(i in 1:length(G_adjs)) # for each partition interaction separately
  {
    # IntOMICS scoring
    # if 1 ---> 1-B(i,j), ...., if 0 ----> B(i,j)
    # E <- sum(G_adjs[[i]] * (1-B_PKs[[i]])  + (1-G_adjs[[i]]) * B_PKs[[i]])


    # BP(Isci) scoring
    # if 1 ---> 1-B(i,j), ...., if 0 ----> 1
    E <- sum(1 - B_PKs[[i]] * G_adjs[[i]]) + E

    # Add non-blocked
    E_non_blocked <- E_non_blocked + ncol(B_PKs[[i]]) * nrow(B_PKs[[i]])

  }

  # in both scoring we add +1 for each blocked edge too
  # to compute it we just compute non-blocked and then:
  # [TOTAL] - [non-BLOCKED] = [BLOCKED] = N^2 - [non-BLOCKED]
  E <- (E + N*N - E_non_blocked)

  return(E)
}
