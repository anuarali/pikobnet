#' @name countgmifs
#' @title Discrete Response Generalized Monotone Incremental Forward Stagewise Regression.
#' @description
#'
#' This function can fit a Poisson or negative binomial model when the number of parameters exceeds the sample size, using the the generalized monotone incremental forward stagewise method.\cr\cr
#'
#' THIS IS A CODE COPIED FROM OPEN-SOURCE LIBRARY \emph{countgmifs}:\cr
#' \url{https://github.com/cran/countgmifs}\cr
#' IT WAS SUBSEQUENTLY MODIFIED ACCORDING TO THE NEED OF A RESEARCH:\cr\cr
#'
#' \emph{PiKOBNet, change 1 (major, technical)}: Manual technical adjustments were performed to improve the algorithm in case of enormous and unstable count data input. Particularly -- a problem with \code{MASS::glm.nb(...)} has been observed.
#' A solution is to try to run it with maxit parameter decrease until it converges (no NA values). If not possible, run \code{glmmTMB::glmmTMB(...)} approach instead. Differences were found to be insignificant, both libraries result in roughly the same output.\cr\cr
#'
#' \emph{PiKOBNet, change 2 (minor, technical)}: Instead of adding each subsequent iteration coefficients as a new row to a matrix, which involves entire matrix data copy, run the algorithm with list that is being appended. At the end convert it using \code{do.call(rbind,...)}\cr\cr
#'
#' \emph{PiKOBNet, change 3 (theoretical addition)}: Stopping criterion introduced to stop the iterative procedure in case of oscillation\cr
#'
#' @param formula an object of class "\code{formula}" (or one that can be coerced to that class): a symbolic description of the model to be fitted. The left side of the formula is the ordinal outcome while the variables on the right side of the formula are the covariates that are not included in the penalization process. Note that if all variables in the model are to be penalized, an intercept only model formula should be specified.
#' @param data an optional data frame, list or environment (or object coercible by \code{as.data.frame} to a data frame) containing the variables in the model.
#' @param x an optional matrix of predictors that are to be penalized in the model fitting process.
#' @param offset this can be used to specify an a priori known component to be included during fitting (e.g., denominator term). This should be NULL or a numeric vector of length equal to the number of cases.
#' @param subset an optional vector specifying a subset of observations to be used in the fitting process.
#' @param epsilon small incremental amount used to update a coefficient at a given step.
#' @param tol the iterative process stops when the difference between successive log-likelihoods is less than this specified level of tolerance.
#' @param scale logical, if TRUE (default) the penalized predictors are centered and scaled.
#' @param verbose logical, if TRUE the step number is printed to the console (default is FALSE).
#' @param family the type of count response model to be fit. Default is 'nb' for negative binomial; user can also specify 'poisson'.
#' @param maxit.cutoff A new criterion added to prevent indefinite oscillations or long runs. A maximum number of iterations that algorithm is allowed to traverse without improving the maximum likelihood or not including additional non-zero coefficient
#' @param dots other arguments.
#' @keywords methods
#' @keywords regression
#' @export
#' @examples
#' countgmifs()
countgmifs<-function (formula, data, x=NULL, offset, subset, epsilon=0.001, tol=1e-5, scale=TRUE, verbose=FALSE,
                      family = "nb", maxit.cutoff = 3000,
                      ...) {
    mf <- match.call(expand.dots = FALSE)
    cl <- match.call()
    m <- match(c("formula", "data", "subset", "offset"), names(mf), 0L)
    mf <- mf[c(1L, m)]
    mf[[1L]] <- as.name("model.frame")
    mf <- eval(mf, parent.frame())
    mt <- attr(mf, "terms")
    y <- model.response(mf)
    w <- model.matrix(mt, mf)
    offset <- model.offset(mf)
    #### Subset code
    if (!is.null(x)) {
        if (missing(subset))
            r <- TRUE
        else {
            e <- substitute(subset)
            r <- eval( e, data)
            if (!is.logical(r))
                stop("'subset' must evaluate to logical" )
            r <- r & !is.na(r)
        }
        if (sum(class(x)%in%"character")==1) {
            nl <- as.list( 1:ncol(data))
            names(nl) <- names( data)
            vars <- eval(substitute(x), nl, parent.frame())
            x <- data [r , vars, drop=FALSE ]
            x <- as.matrix(x )
        } else if ( sum(class(x)%in%"matrix")==1 || sum(class(x)%in%"data.frame")==1) {
            x <- x[r,, drop =FALSE]
            x <- as.matrix(x)
        }
    }
    #### End subset code
    if (is.na(match(family, c("poisson", "nb")))) {
        stop("Error:")
        cat("Only poisson and nb are available for family.\n")
    }
    if(!is.null(offset)){
        offset<- log(offset)
    }
    data <- data.matrix(data)
    n<- length(y)
    if (!is.null(x)) {
        vars <- dim(x)[2]
        # vars is the number of penalized variables
        oldx <- x
        if (scale) {
            x <- scale(x, center = TRUE, scale = TRUE)
            # Center and scale the penalized variables
        }
        x_original<-x
        # Keep the old x, will use in Hilbe's and estimation of theta
        x <- cbind(x, -1 * x)
        # x is now the expanded x matrix
        beta <- rep(0, dim(x)[2])
        # Beta as a vector of 0's with a length equivalent the the expanded x
        names(beta) <- dimnames(x)[[2]]
        step <- 1
        Estimates <- matrix(0,ncol=vars)
        # Estimates will be the final collapsed beta values- matrix
        if (family=="nb") {

            initialize <- NULL
            maxit.try = 100
            while(is.null(initialize)){
                cat("maxit.glm initialize: ", maxit.try, "\n")
                smart.init <- function(){
                    tryCatch({
                        if(!is.null(offset)){
                            initialize<-
                                MASS::glm.nb(y~w-1 + offset(offset), control=glm.control(maxit=maxit.try, trace=T))
                            # Starting values theta and (Intercept)
                        } else {
                            initialize<-MASS::glm.nb(y~w-1,control=glm.control(maxit=maxit.try, trace=T))
                        }
                        return(initialize)
                    },
                    error=function(err) {
                        print(err)
                        NULL
                    })
                }

                initialize <- smart.init()

                maxit.try <- maxit.try - 1
                if(maxit.try == 0){

                    initialize <- glmmTMB::glmmTMB(y~w-1+offset(offset),
                                           family=glmmTMB::nbinom2)
                }
            }
            initialize2 <- glmmTMB::glmmTMB(y~w-1+offset(offset),
                                   family=glmmTMB::nbinom2)

            a<- 1/theta.mm(initialize)
            # Alpha for model with no penalized predictors,
            # use mm estimate to initialize
            a.update<- a
        } else {
            if(!is.null(offset)){
                initialize<-glm(y~w-1, offset=offset, family=poisson)
            } else {
                initialize<-glm(y~w-1, family=poisson)
            }
        }
        LL0 <- Likelihood <- logLik(initialize)
        # Log-likelihood for model with no penalized predictors
        AIC<-AIC(initialize)
        # AIC for model with no penalized predictors
        BIC<-BIC(initialize)
        # BIC for model with no penalized predictors
        if(class(initialize)[1] == "glmmTMB")
            theta <- glmmTMB::fixef(initialize)$cond
        else
            theta <- coef(initialize)

        if(verbose){
            cat("initial class: ")
            print(class(initialize))
            cat("initial BIC via : ", BIC, "\n")
            cat("initial BIC via glmmTMB: ", BIC(initialize2), "\n")
            cat("initial theta: ", theta, "\n")
            cat("glmmTMB: ", glmmTMB::fixef(initialize2)$cond, "\n")
        }

        # Unpenalized coefficient estimates for model
        # with no penalized predictors
        theta.update <- matrix(theta, ncol = length(theta))
        # a.update will be used to keep track of all alpha estimates

        prev.p <- 0
        step.p <- step
        prev.Ldiff <- 0
        maxLikelihood = LL0

        Estimates <- list(Estimates)
        repeat {
            step <- 1 + step
            step.p <- 1 + step.p
            # Xb will be calculated depending on whether offset is present
            # and whether there are penalized variables
            if (!is.null(offset)) {
                Xb <- cbind(offset, w, x) %*% c(1, theta, beta)
            } else {
                Xb <- cbind(w, x) %*% c(theta, beta)
            }
            if (family=="nb") {
                u <- t(x) %*% ((y- exp(Xb)) /(1+ (a*exp(Xb))))
            } else {
                u <- t(x)%*%(y-exp(Xb))
            }
            # Likelihood gradient value- NEGATIVE BINOMIAL Hilbe Page 192
            update.j <- which.min(-u)
            # Choose coeffiecient to update
            if (-u[update.j] < 0) {
                beta[update.j] <- beta[update.j] + epsilon
                # Update beta
            }
            #Estimates<-rbind(Estimates,beta[1:vars]-beta[(vars+1):length(beta)])
            Estimates[[step]] <- beta[1:vars]-beta[(vars+1):length(beta)]
            # Keep track of beta changes
            # Update intercept and non-penalized subset using new beta values
            if (family=="nb") {
                out <- tryCatch(
                    optim(theta, countgmifs:::nb.theta, a=a, w=w,
                          x=x_original, y=y, offset=offset,
                          beta=beta[1:vars]-beta[(vars+1):length(beta)],
                          method="BFGS"),
                    error = function(err) {
                        print(err)
                        return(NULL)
                    })

                if(is.null(out)){
                    out <- optim(theta, countgmifs:::nb.theta, a=a, w=w,
                                 x=x_original, y=y, offset=offset,
                                 beta=beta[1:vars]-beta[(vars+1):length(beta)], method="SANN")
                }
                theta <- out$par

                # Update alpha using Hilbe algorithm. Need to use the original x not the expanded
                a<- countgmifs:::hilbe(w=w,y=y,x=x_original,theta=theta, beta=beta[1:vars]-beta[(vars+1):length(beta)], offset= offset, delta=1e-5)
                a.update<- c(a.update,a)
                # Number of predictors in the NB model: nonzero beta + theta + alpha(1)
                p <- sum(Estimates[[step]]!=0) + length(theta) + 1
                Likelihood[step]<- LL1<- -out$value
                AIC[step]<- 2*p-2*Likelihood[step]
                BIC[step]<- p*log(n) - 2*Likelihood[step]
            } else {
                out <- optim(theta, poisson.theta, w=w, x=x, y=y, offset=offset, beta=beta, method="BFGS")
                theta <- out$par
                p <- sum(Estimates[[step+1]]!=0) + length(theta)
                Likelihood[step]<- LL1<- -out$value
                AIC[step]<- 2*p-2*Likelihood[step]
                BIC[step]<- p*log(n) - 2*Likelihood[step]
            }
            # Keep track of theta values
            theta.update <- rbind(theta.update, theta)
            #if (!is.null(offset)) {
            #	# Calculate Xb to be used to calculate the Likelihood
            #	Xb_LL <- cbind(offset, w, x_original) %*%c(1, theta, beta[1:vars]-beta[(vars+1):length(beta)])
            #} else {
            #	Xb_LL <- cbind(w, x_original) %*%c(theta,beta[1:vars]-beta[(vars+1):length(beta)])
            #}
            #Likelihood[step]<-LL1<- sum(y*log((a*exp(Xb_LL))/(1+ (a*exp(Xb_LL)))) - (1/a)*log(1+ (a*exp(Xb_LL))) + lgamma(y+ (1/a)) - lgamma(y+1) - lgamma(1/a))
            ## likelihood function- NEGATIVE BINOMIAL Hilbe pg 190
            #AIC[step] <- 2*p - 2*Likelihood[step]
            ## AIC - equation 5.16 Hilbe pg 68
            #BIC[step] <- p*log(n) - 2*Likelihood[step]
            ## BIC - equation 5.21 Hilbe pg 71
            if (verbose){
                cat("step = ", step, "\n")
                cat("LL diff: ",abs(LL1- LL0), ", tol:", tol, "\n")
                cat("nonzero vars p:", p, "\n")
                cat("step.p: ", step.p, "\n")
                cat("Epsilon: ", epsilon, "\n")
                cat("LL1: ", LL1, "\n")
                print(sprintf("MaxLikelihood: %.10f", maxLikelihood, "\n"))
            }
            Ldiff = abs(LL1- LL0)

            # STOPPING CRITERIA
            if (step >= 1 && ( (abs(LL1- LL0)<tol) ||(p>=n-1) )) {
                break
            }
            LL0 <- LL1

            # ADDITIONAL STOPPING CRITERIA TO PREVENT OSCILLATION
            if(step.p > maxit.cutoff){
                break
            }

            if((p > prev.p) || (maxLikelihood < LL1)){
                prev.p = max(c(p, prev.p))
                step.p = 0
                if(maxLikelihood < LL1)
                    maxLikelihood = LL1
            }
            prev.Ldiff = Ldiff
            # Assign the "old" LL value the "new" LL value for the next step
        }
        Estimates = do.call("rbind", Estimates)
        if (family=="nb") {
            output<-list(a=a.update, beta = Estimates, theta=theta.update, x=oldx, y=y, scale=scale, logLik=Likelihood, AIC=AIC, BIC=BIC,w=w,offset=offset, family=family)
        } else {
            output<-list(beta = Estimates, theta=theta.update, x=oldx, y=y, scale=scale, logLik=Likelihood, AIC=AIC, BIC=BIC, w=w,offset=offset, family=family)
        }
        class(output) <- "countgmifs"
    } else {
        if (family=="nb") {
            out<-glm.nb(y~w-1, offset=offset)
            output <- list(coef(out), a=1/out$theta)
        } else {
            output<-glm(y~w-1, offset=offset, family=poisson)
        }
    }

    output
}


theta.mm <- function(y, mu, dfr, weights, limit = 10,
                     eps = .Machine$double.eps^0.25)
{
    if(inherits(y, "lm")) {
        mu <- y$fitted.values
        dfr <- y$df.residual
        y <- if(is.null(y$y)) mu + residuals(y) else y$y
    }
    if(class(y) == "glmmTMB"){
        mu <- fitted.values(y)
        dfr <- df.residual(y)+1
        y <- if(is.null(y$y)) mu + residuals(y) else y$y
    }
    if(missing(weights)) weights <- rep(1, length(y))
    n <- sum(weights)
    t0 <- n/sum(weights*(y/mu - 1)^2)
    it <- 0
    del <- 1
    while((it <- it + 1) < limit && abs(del) > eps) {
        t0 <- abs(t0)
        del <- (sum(weights*((y - mu)^2/(mu + mu^2/t0))) - dfr)/
            sum(weights*(y - mu)^2/(mu + t0)^2)
        t0 <- t0 - del
    }
    if(t0 < 0) {
        t0 <- 0
        warning("estimate truncated at zero")
        attr(t0, "warn") <- gettext("estimate truncated at zero")
    }
    t0
}


#' Predict Outcome for Count GMIFS Fitted Model.
#'
#' This function returns a numeric vector that is the predicted response from the \code{countgmifs} fitted object.
#' @param object an \code{ordinalgmifs} fitted object.
#' @param neww an optional formula that includes the unpenalized variables to use for predicting the response. If omitted, the training data are used.
#' @param newdata an optional data.frame that minimally includes the unpenalized variables to use for predicting the response. If omitted, the training data are used.
#' @param newx an optional matrix of penalized variables to use for predicting the response. If omitted, the training data are used.
#' @param model.select when \code{x} is specified any model along the solution path can be selected. The default is \code{model.select="BIC"} which calculates the predicted values using the coefficients from the model having the lowest BIC. Other options are \code{model.select="AIC"} or any numeric value from the solution path.
#' @param dots other arguments.
#' @keywords methods
#' @export
#' @examples
#' predict.picountgmifs()
predict.countgmifs <- function(object, neww = NULL, newdata, newx = NULL, model.select="BIC", newoffset=NULL, ...) {

    model.select = sapply(model.select, function(val){
        if(val == "AIC"){
            return(which.min(object$AIC[-1]))
        }
        if(val == "BIC"){
            return(which.min(object$BIC[-1]))
        }
        val
    })

    y <- object$y
    w <- object$w
    x <- object$x
    if (!is.null(newx)) {
        if (class(newx)[1]=="data.frame") newx<-as.matrix(newx)
        identical.x<-all.equal(object$x, newx, check.attributes=FALSE)
    }
    family <- object$family
    offset<-object$offset
    if (!is.null(newx) && !is.null(offset) && is.null(newoffset)) stop("newoffset must be specified because offset was specified in the model fit")
    new.w <- neww
    if (is.null(neww) || neww==~1) {
        if (!is.null(newx)) {
            neww <- as.matrix(w[1:dim(newx)[1],drop=FALSE])
        } else {
            neww <- w
        }
    } else {
        m <- model.frame(neww, newdata)
        neww <- model.matrix(neww, m)
    }
    if (!is.null(newx)) {
        if (is.logical(identical.x)) {
            if (object$scale) {
                sd <- apply(newx, 2, sd)
                for (i in 1:dim(newx)[2]) {
                    if (sd[i] == 0) {
                        newx[, i] <- scale(newx[, i], center = TRUE,
                                           scale = FALSE)
                    }
                    else {
                        newx[, i] <- scale(newx[, i], center = TRUE,
                                           scale = TRUE)
                    }
                }
            }
        } else if (object$scale) {
            newx <- rbind(x, newx)
            newx2 <- newx
            sd <- apply(newx, 2, sd)

            newx <- sapply(1:dim(newx)[2], function(i){
                if (sd[i] == 0) {
                    ret <- scale(newx[, i], center = TRUE,
                                 scale = FALSE)
                } else {
                    ret <- scale(newx[, i], center = TRUE,
                                 scale = TRUE)
                }
                return(ret)
            })
            #newx <- do.call("cbind", newx)
            colnames(newx) <- colnames(newx2)
            rownames(newx) <- rownames(newx2)

            for (i in 1:dim(newx2)[2]) {
                if (sd[i] == 0) {
                    newx2[, i] <- scale(newx2[, i], center = TRUE,
                                        scale = FALSE)
                } else {
                    newx2[, i] <- scale(newx2[, i], center = TRUE,
                                        scale = TRUE)
                }
            }
            stopifnot(identical(newx2, newx))
            newx <- newx[-(1:dim(x)[1]),,drop=FALSE]
        }
    }
    #    if (dim(neww)[2]!=dim(w)[2]) stop("neww must include the same number of covariates as the original model")
    #    if (!is.null(newx) && dim(newx)[2]!=dim(x)[2]) stop("newx must include the same number of covariates as the original model")
    #    # scale newx if scale is TRUE
    if (!is.null(newx)) {
        beta <- object$beta[model.select,]
        if ( dim(w)[2]==1 ) {
            theta <- object$theta[model.select]
        } else {
            theta <- object$theta[model.select,]
        }
        if (!is.null(newoffset)) {
            #Xb <- cbind(newoffset, neww, newx) %*% t(cbind(1, theta, beta))
        } else {
            #Xb <- cbind(neww, newx) %*% t(cbind(theta, beta)) # This is the error, there is no neww
            newoffset <- offset
        }
        Xb <- cbind(t(newoffset), neww, newx) %*% t(cbind(1, theta, beta))
    }  else if (is.null(newx) & is.null(new.w)) {
        neww <- w
        newx <- x
        if (object$scale) {
            sd <- apply(newx, 2, sd)
            for (i in 1:dim(newx)[2]) {
                if (sd[i] == 0) {
                    newx[, i] <- scale(newx[, i], center = TRUE,
                                       scale = FALSE)
                }
                else {
                    newx[, i] <- scale(newx[, i], center = TRUE,
                                       scale = TRUE)
                }
            }
        }
        beta <- object$beta[model.select,,drop=F]
        if ( dim(w)[2]==1 ) {
            theta <- object$theta[model.select]
            theta <- matrix(theta, ncol=1)
        } else {
            theta <- object$theta[model.select,,drop=F]
        }
        if (!is.null(newoffset)) {
            #Xb <- cbind(newoffset, neww, newx) %*% t(cbind(1, theta, beta))
        } else {
            newoffset <- offset
            #Xb <- cbind(neww, newx) %*% t(cbind(theta, beta))
        }
        Xb <- cbind(t(newoffset), neww, newx) %*% t(cbind(1, theta, beta))
    } else if (is.null(newx) && !is.null(new.w)) {
        if (!is.null(newoffset)) {
            newoffset <- offset
            #Xb <- cbind(newoffset, neww) %*% t(cbind(1, theta))
        } else {
            #Xb <- neww %*% t(theta)
        }
        Xb <- cbind(t(newoffset), neww) %*% t(cbind(1, theta))
    }
    y.pred <- exp(Xb)

    if((length(dim(y.pred)) == 2) &&
       (ncol(y.pred) > 1)){
        storage.mode(y.pred) <- "double"
    }
    else{
        y.pred <- as.numeric(y.pred)
    }
    y.pred
}

#' @title Predict coefficients from picountgmifs object
#' @name testpredict.loglik.picountgmifs
#'
#' @export
testpredict.picountgmifs <- function(model, y_in, ...){
    model.select = 1:nrow(model$beta)

    # computed by library
    #libLoglik <- model$logLik[model.select]

    # manual
    #y_in <- model$y
    y_hat <- predict(object=model, model.select=model.select,...)
    a_hat <- 1 / model$a[model.select]

    if(length(dim(y_hat)) == 2){
        manualLoglik = sapply(1:ncol(y_hat), function(i){
            sum(dnbinom(y_in, mu=y_hat[,i], size=a_hat[i], log = TRUE))
        })
    }
    else{
        manualLoglik <- sum(dnbinom(y_in, mu=y_hat, size=a_hat, log = TRUE))
    }

    return(manualLoglik)
}
