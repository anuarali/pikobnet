# Prior-incorporated Known Ordering Bayesian Network (PiKOBNet)

## Installation
```
library(devtools)
devtools::install_git("https://gitlab.fel.cvut.cz/anuarali/pikobnet.git")
```

Note: If it is the first run of devtools and you have installed the RTools, then restart RStudio/R before running the command above. 


Note 2: If some other errors are present, try adding R folder to your PATH variable list


## Prior-incorporated Generalized Monotone Incremental Forward Stagewise (Pi-GMIFS)
A documentation is provided via R standard helper:
```
?pi.countgmifs
```
A typical usage example:
```
library(PiKOBNet)
example = PiKOBNet:::experiment.5mrna()
pi.countgmifs(data = example$data, PK = example$PK, partition = example$partition, family = "nb", geneLength = example$geneLength, normalization.type = "CPM", folder="testFolder", verbose = T, etas=1)
coef.picountgmifs(folder = "testFolder/")

```

## Interesting functions
```
?rnaseq_datasets
?dataset_MDS
?PK_tarbase9
?PK_multimir
?data50
?data400
?generate_data 
?mmpc_tripartite
?SK_SS_tripartite
```
